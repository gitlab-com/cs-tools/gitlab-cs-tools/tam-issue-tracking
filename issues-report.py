#!/usr/bin/env python3

import gitlab
import json
import csv
import os
import argparse
import re
import urllib.parse
import time
import yaml
import requests
import datetime
from tracking_helper import Tracking_Helper
from datetime import datetime, timedelta
from mako.template import Template
from distutils import util

class Issue_Reporter():

    gitlab_url = "https://gitlab.com"
    api_url = "https://gitlab.com/api/v4/"
    #gitlab-org/gitlab, where most issues are moved to
    gitlab_org_project = None
    gitlab_moved_issues_map = None
    project_metadata = None
    headers = None
    gl = None
    token = None
    headers = None
    project = None
    issuefile = None
    startdate = None
    customer_tracking_issue = None
    customer_project_name = ""
    INACTIVITY_PERIOD = 30
    pages_link = ""
    path = os.path.abspath(os.path.dirname(__file__))

    def __init__(self, args):
        if args.gitlab:
            self.gitlab_url = args.gitlab if args.gitlab.endswith("/") else args.gitlab + "/"
            self.api_url = self.gitlab_url + "api/v4/"
        self.token = args.token
        self.gl = gitlab.Gitlab(self.gitlab_url, private_token=self.token)
        self.headers = {'PRIVATE-TOKEN': args.token}
        self.project = self.gl.projects.get(args.project)
        if args.customer:
            self.customer_project_name = args.customer
        else:
            self.customer_project_name = self.project.attributes["path_with_namespace"][self.project.attributes["path_with_namespace"].rfind("/")+1:]

        namespace = self.project.namespace["full_path"]
        if "/" in namespace:
            self.pages_link = "https://%s.gitlab.io/%s/%s/index.html" % (namespace[0:namespace.find("/")], namespace[namespace.find("/")+1:], self.project.path)
        else:
            self.pages_link = "https://%s.gitlab.io/%s/index.html" % (namespace, self.project.path)
        self.gitlab_org_project = self.gl.projects.get(278964)
        self.headers = {'PRIVATE-TOKEN': args.token}
        self.issuefile = args.issuefile
        try:
            if args.cadence:
                self.startdate = datetime.now() - timedelta(days=args.cadence)
            else:
                print("No start date specified, looking for updates between 30 days ago and now")
                self.startdate = datetime.now() - timedelta(days=30)
        except:
            print("Can't parse start date: %s" % args.startdate)
            exit(1)
        # I can't believe I'm doing this
        # Parse a map of gitlab issues so I can get issues by id to deal with moved issues
        with open(self.path + "/gitlab_issue_map.json","r") as infile:
            self.gitlab_moved_issues_map = json.load(infile)

    def parse_issue_yml(self, issuefile):
        issue_dict = {}
        project_map = {}
        with open(issuefile, "r") as issuefile:
            issues = yaml.safe_load(issuefile)
            for issue in issues["issues"]:
                issue_data = {}
                # simply crawling again, currently not using existing issue data in yml file
                # TODO: use yml data to highlight changes
                issue_data["object"] = self.fetch_issue(issue, project_map)
                if not issue_data["object"]:
                    continue
                if issues["issues"][issue]:
                    issue_data["date_logged"] = str(issues["issues"][issue]["date_logged"]) if issues["issues"][issue].get("date_logged") else ""
                    issue_data["notes"] = issues["issues"][issue]["notes"] if "notes" in issues["issues"][issue] else ""
                    issue_data["urgency"] = issues["issues"][issue]["urgency"] if "urgency" in issues["issues"][issue] else 1
                    issue_data["value"] = issues["issues"][issue]["value"] if "value" in issues["issues"][issue] else 1
                    if "priority" in issues["issues"][issue]:
                        issue_data["urgency"] = issues["issues"][issue]["priority"]
                        issue_data["value"] = issues["issues"][issue]["priority"]
                else:
                    issue_data["date_logged"] = ""
                    issue_data["notes"] = ""
                    issue_data["urgency"] = 1
                    issue_data["value"] = 1
                issue_dict[issue] = issue_data
            tracking_issue = self.project.issues.list(labels=['product-issues'])
            if tracking_issue:
                self.customer_tracking_issue = tracking_issue[0]
            else:
                print("No issue labelled 'product-issues', can not track issues.")
                exit(1)
            self.project_metadata = issues["metadata"]
            if "customer_name" not in self.project_metadata:
                self.project_metadata["customer_name"] = self.customer_project_name
        return issue_dict

    def write_json(self, issues):
        json_issues = []
        for issue in issues.values():
            json_issue = issue.copy()
            json_issue["object"] = json_issue["object"].attributes
            json_issues.append(json_issue)
        customer = {}
        customer["metadata"] = self.project_metadata
        customer["issues"] = json_issues
        with open(self.path + "/public/customer_issues.json","w", encoding="utf-8") as outfile:
            outfile.write(json.dumps(customer, indent=4, ensure_ascii=False))

    def write_flat_json(self, issues):
        with open(self.path + "/public/customer_issues_flat.json","w", encoding="utf-8") as outfile:
            outfile.write(json.dumps(issues, indent=4, ensure_ascii=False))

    def write_overview_html(self, issues):
        #crude way of getting the fields of the first issue object
        fields = issues[0].keys()
        index_template = Template(filename=self.path + '/template/index.html')
        with open(self.path + "/public/index.html","w") as outfile:
            try:
                outfile.write(index_template.render(issues = issues, fields = fields, customer_project = self.customer_project_name, startdate = str(self.startdate.date().isoformat()), enddate = str(datetime.now().date().isoformat())))
            except:
                print("Could not render index HTML")

    def write_stats_html(self, issuese):
        stats_template = Template(filename=self.path + '/template/stats.html')
        with open(self.path + "/public/stats.html","w") as outfile:
            try:
                outfile.write(stats_template.render(customer_project = self.customer_project_name, startdate = str(self.startdate.date().isoformat()), enddate = str(datetime.now().date().isoformat())))
            except:
                print("Could not render stats HTML")

    def flatten(self, issues):
        customer_issues = []
        for issue in issues.values():
            issue_object = issue["object"]
            customer_issue = {}
            customer_issue["title"] = issue_object.attributes["title"]
            customer_issue["url"] = issue_object.attributes["web_url"] + "/"
            customer_issue["labels"] = issue_object.attributes["labels"]
            customer_issue["milestone"] = ""
            if issue_object.attributes["milestone"]:
                customer_issue["milestone"] = issue_object.attributes["milestone"]["title"]
            customer_issue["state"] = issue_object.attributes["state"]
            customer_issue["urgency"] = issue["urgency"]
            customer_issue["value"] = issue["value"]
            customer_issue["notes"] = issue["notes"]
            customer_issue["date_logged"] = issue["date_logged"]
            customer_issue["is_stale"] = issue["is_stale"]
            customer_issue["inactivity_days"] = issue["inactivity_days"]
            customer_issue["updated_at"] = self.datetime_to_isostring(issue_object.attributes["updated_at"])
            customer_issue["created_at"] = self.datetime_to_isostring(issue_object.attributes["created_at"])
            customer_issue["closed_at"] = self.datetime_to_isostring(issue_object.attributes["closed_at"])
            customer_issue["type"] = issue["type"]
            customer_issue["milestone_due"] = issue["milestone_due"]
            customer_issue["missed"] = issue["missed"]
            customer_issue["scheduled"] = issue["scheduled"]
            customer_issue["stage"] = issue["stage"]
            customer_issue["cycle_time_created"] = issue["cycle_time_created"]
            customer_issue["cycle_time_logged"] = issue["cycle_time_logged"]
            customer_issues.append(customer_issue)
        return customer_issues

    def datetime_to_isostring(self, datetime_string):
        isostring = ""
        try:
            isostring = str(datetime.strptime(datetime_string, '%Y-%m-%dT%H:%M:%S.%fZ').date().isoformat())
        except:
            pass
        return isostring

    def resolve_moved_issue(self, issue):
        if issue.attributes["moved_to_id"]:
            if str(issue.attributes["moved_to_id"]) in self.gitlab_moved_issues_map:
                target_issue_iid = self.gitlab_moved_issues_map[str(issue.attributes["moved_to_id"])]
                target_issue_object = self.gitlab_org_project.issues.get(target_issue_iid)
                issue = target_issue_object
        return issue

    # get python-gitlab issue object, no matter from which project (even ce/ee), resolve moved issues
    def fetch_issue(self, issue_url, project_map):
        if "gitlab-org" not in issue_url:
            return None
        issue_url = issue_url[0:-1] if issue_url.endswith("/") else issue_url
        project_id = issue_url[issue_url.find("gitlab-org/"):issue_url.rfind("issues")-1].replace("/-","").replace("gitlab-ee","gitlab").replace("gitlab-ce","gitlab-foss")
        parsed_issue_id = issue_url[issue_url.rfind("/")+1:]
        try:
            project = None
            if project_id not in project_map:
                project = self.gl.projects.get(project_id)
                project_map[project_id] = project
            else:
                project = project_map[project_id]

            issue_object = project.issues.get(parsed_issue_id)
            #handle moved issues
            issue_object = self.resolve_moved_issue(issue_object)
            return issue_object
        except Exception as e:
            print("ERROR: {0}".format(e))
            print("Project: %s" % project_id)
            print("GitLab issue not found, may be confidential: %s" % issue_url)
            return None

    def filter_duplicates(self, issue_objects):
        unique_urls = set()
        unique_issues = []
        for issue in issue_objects:
            if issue.attributes["web_url"] not in unique_urls:
                unique_urls.add(issue.attributes["web_url"])
                unique_issues.append(issue)
        return unique_issues

    def mark_stale(self, customer_issues):
        '''
        INACTIVITY_PERIOD days no activity = mark issue stale
        TODO: Can not use "updated_at" because when I mention it in the notifications, I update it by mentioning, thus resetting the staleness
        Some real Heisenberg kinda crap right there.
        For now will have the staleness not link the issue
        # parse notes since date, if type is not null, update
        # changed the description
        # mentioned in
        '''
        for issue in customer_issues.values():
            issue_object = issue["object"]
            if issue_object.state != "closed":
                tz = datetime.now().astimezone().tzinfo
                no_activity_time = datetime.now(tz) - datetime.strptime(issue_object.updated_at, '%Y-%m-%dT%H:%M:%S.%f%z')
                issue["inactivity_days"] = no_activity_time.days
                if no_activity_time.days > self.INACTIVITY_PERIOD:
                    issue["is_stale"] = "True"
                else:
                    issue["is_stale"] = "False"
            else:
                issue["inactivity_days"] = None
                issue["is_stale"] = "False"
        return customer_issues

    def categorize_issues(self, customer_issues):
        for issue in customer_issues.values():
            issue["type"] = ""
            issue["milestone_due"] = None
            issue["missed"] = 0
            issue["scheduled"] = False
            issue["stage"] = ""
            issue_object = issue["object"]
            labels = issue_object.labels
            for label in labels:
                if label.lower().strip() == "bug":
                    issue["type"] = "bug"

                if "missed:" in label.lower().strip():
                    issue["missed"] += 1

                if label.lower().strip().startswith("devops:"):
                    issue["stage"] = label[label.find(":")+2:]

            # if it's not a bug, it's a feature!
            if not issue["type"]:
                issue["type"] = "feature"

            milestone = issue_object.milestone
            if milestone is not None:
                issue["milestone_due"] = milestone["due_date"]
                if issue["milestone_due"]:
                    issue["scheduled"] = True

            if issue_object.state == "closed":
                # closed_at sometimes none for old issues
                if issue_object.closed_at:
                    cycle_time_create = datetime.strptime(issue_object.closed_at, '%Y-%m-%dT%H:%M:%S.%fZ') - datetime.strptime(issue_object.created_at, '%Y-%m-%dT%H:%M:%S.%fZ')
                    issue["cycle_time_created"] = cycle_time_create.days
                else:
                    issue["cycle_time_created"] = None
                if issue["date_logged"] and issue_object.closed_at:
                    cycle_time_logged = datetime.strptime(issue_object.closed_at, '%Y-%m-%dT%H:%M:%S.%fZ') - datetime.strptime(issue["date_logged"], '%Y-%m-%d')
                    issue["cycle_time_logged"] = cycle_time_logged.days
                else:
                    issue["cycle_time_logged"] = None
            else:
                issue["cycle_time_created"] = None
                issue["cycle_time_logged"] = None

        return customer_issues

    def add_updates(self, issues):

        for issue in issues.values():
            issue_object = issue["object"]
            issue["scheduled_at"] = None
            updates = { "milestone_events":[], "label_events":[], "state_events":[]}

            try:
                milestone_events = requests.get("%sprojects/%s/issues/%s/resource_milestone_events?per_page=100" % (self.api_url, issue_object.project_id, issue_object.iid), headers=self.headers)
                for milestone_event in milestone_events.json():
                    milestone_date = datetime.strptime(milestone_event["created_at"], '%Y-%m-%dT%H:%M:%S.%fZ')
                    if milestone_date > self.startdate:
                        updates["milestone_events"].append(milestone_event)
            except Exception as e:
                print("Could not parse milestone events: %s" % e)

            try:
                label_events = requests.get("%sprojects/%s/issues/%s/resource_label_events?per_page=100" % (self.api_url, issue_object.project_id, issue_object.iid), headers=self.headers)
                for label_event in label_events.json():
                    label_date = datetime.strptime(label_event["created_at"], '%Y-%m-%dT%H:%M:%S.%fZ')
                    if label_date > self.startdate:
                        updates["label_events"].append(label_event)
            except Exception as e:
                print("Could not parse label events: %s" % e)

            try:
                state_events = requests.get("%sprojects/%s/issues/%s/resource_state_events?per_page=100" % (self.api_url, issue_object.project_id, issue_object.iid), headers=self.headers)
                for state_event in state_events.json():
                    state_date = datetime.strptime(state_event["created_at"], '%Y-%m-%dT%H:%M:%S.%fZ')
                    if state_date > self.startdate:
                        updates["state_events"].append(state_event)
            except Exception as e:
                print("Could not state label events: %s" % e)

            if updates["milestone_events"] or updates["label_events"] or updates["state_events"]:
                issue["updates"] = updates
            else:
                issue["updates"] = {}
        return issues

    def post_updates_note(self, issues):
        #self.customer_tracking_issue
        note_text = ""
        has_updates = False
        milestone_updates = set()
        label_updates = set()
        status_updates = set()
        stale_issues = set()
        for issue in issues:
            if "milestone_events" in issues[issue]["updates"]:
                milestone_updates.add(issue)
            if "label_events" in issues[issue]["updates"]:
                label_updates.add(issue)
            if "state_events" in issues[issue]["updates"]:
                status_updates.add(issue)
            if issues[issue]["inactivity_days"]:
                if issues[issue]["inactivity_days"] > self.INACTIVITY_PERIOD:
                    stale_issues.add(issue)

        if status_updates:
            status_text = "## State\n"
            for issue in status_updates:
                updates = issues[issue]["updates"]["state_events"]
                for update in updates:
                    issue_url = issues[issue]["object"].attributes["web_url"]
                    title = issues[issue]["object"].attributes["title"]
                    # do not ping users on update note to not spam their inbox
                    status_text += "[%s](%s): %s changed status to **%s**\n\n" % (title,issue_url,update["user"]["username"],update["state"])
            if status_text != "## State\n":
                note_text += status_text

        if milestone_updates:
            milestone_text = "\n## Milestones\n"
            for issue in milestone_updates:
                updates = issues[issue]["updates"]["milestone_events"]
                for update in updates:
                    issue_url = issues[issue]["object"].attributes["web_url"]
                    title = issues[issue]["object"].attributes["title"]
                    # do not ping users on update note to not spam their inbox
                    milestone_text += "[%s](%s): %s set milestone to **%s** (due %s)\n\n" % (title,issue_url,update["user"]["username"],update["milestone"]["title"],update["milestone"]["due_date"])
            if milestone_text != "\n## Milestones\n":
                note_text += milestone_text

        if label_updates:
            label_text = "\n## Labels\n"
            for issue in label_updates:
                updates = issues[issue]["updates"]["label_events"]
                for update in updates:
                    issue_url = issues[issue]["object"].attributes["web_url"]
                    title = issues[issue]["object"].attributes["title"]
                    action = update["action"] + "ed" if not update["action"].endswith("e") else update["action"] + "d"
                    # do not ping users on update note to not spam their inbox
                    # some labels get deleted, so they are none:
                    if update.get("label"):
                        label_text += "[%s](%s): %s %s label ~\"%s\"\n\n" % (title,issue_url,update["user"]["username"],action,update["label"]["name"])
            if label_text != "\n## Labels\n":
                note_text += label_text

        if stale_issues:
            note_text += "\n## Stale issues\n"
            for issue in stale_issues:
                issue_id = issues[issue]["object"].id
                title = issues[issue]["object"].attributes["title"]
                #issue not linked here because posting a notification creates an update in the issue, thus resetting staleness
                note_text += "%s (%s): inactive for %s days.\n\n" % (title,issue_id,issues[issue]["inactivity_days"])

        if not note_text:
            note_text = "# No updates to customer issues since %s" % str(self.startdate.date().isoformat())
        else:
            note_text = "# Changes since %s\n\n" % str(self.startdate.date().isoformat()) + note_text

        try:
            self.customer_tracking_issue.notes.create({'body': note_text})
        except:
            print("Can not post update to issue %s" % self.customer_tracking_issue.attributes["web_url"])

    def update_description(self, issues):
        description_text = "# %s GitLab Feature Requests \n\n" % self.customer_project_name
        description_text += "**Caution** This issue is auto-generated. If you want to edit issues, please edit the [issues.yml](issues.yml).\n"
        description_text += "For a filterable overview and statistics, see also the [pages site of this project](%s).\n\n" % self.pages_link
        description_text += "| issue | stage | milestone | value | urgency | date logged | status | notes |\n"
        description_text += "| --- | --- | --- | --- | --- | --- | --- | --- |\n"

        open_issues = [ issue for issue in list(issues.values()) if issue["object"].state == "opened" ]
        closed_issues = [ issue for issue in list(issues.values()) if issue["object"].state == "closed" ]

        for issue in self.priority_sort(open_issues):
            milestone = ""
            if issue["object"].milestone:
                milestone = "%s (due %s)" % (issue["object"].milestone["title"],issue["object"].milestone["due_date"])
            description_text += "| [%s](%s) | %s | %s | %s | %s | %s | %s | %s |\n" % (issue["object"].title.replace("|","\|"), issue["object"].web_url, issue["stage"], milestone, issue["value"], issue["urgency"], issue["date_logged"], issue["object"].state, issue["notes"])
        for issue in self.priority_sort(closed_issues):
            milestone = ""
            if issue["object"].milestone:
                milestone = "%s (due %s)" % (issue["object"].milestone["title"],issue["object"].milestone["due_date"])
            description_text += "| [%s](%s) | %s | %s | %s | %s | %s | %s | %s |\n" % (issue["object"].title.replace("|","\|"), issue["object"].web_url, issue["stage"], milestone, issue["value"], issue["urgency"], issue["date_logged"], issue["object"].state, issue["notes"])
        self.customer_tracking_issue.description = description_text
        self.customer_tracking_issue.save()

    def priority_sort(self, issues):
        sorted_issues = sorted(issues, key=lambda k: k['value'] + k['urgency'], reverse=True)
        return sorted_issues

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Create report for GitLab issues')
    parser.add_argument('token', help='API token able to read the requested projects')
    parser.add_argument('project', help='Project ID to crawl for issues and create updates issue')
    parser.add_argument('issuefile', help='File name of the issues file')
    parser.add_argument('cadence', help='Number of days in the past from which updates are tracked.', type=int)
    parser.add_argument('--gitlab', help='GitLab URL, defaults to https://gitlab.com/')
    parser.add_argument('--customer', help='Name of customer. Defaults to last part of project path')
    parser.add_argument('--updatesNote', help='Add an updates note to the tracking issue. Defaults to true.', type=lambda x:bool(util.strtobool(x)))
    args = parser.parse_args()
    reporter = Issue_Reporter(args)
    issues = None
    if not reporter.issuefile:
        print("No issues file found, trying to find product issues in issue description.")
        exit(1)
    else:
        issues = reporter.parse_issue_yml(reporter.issuefile)
        issues = reporter.add_updates(issues)
        issues = reporter.mark_stale(issues)
        issues = reporter.categorize_issues(issues)
        reporter.update_description(issues)
        if args.updatesNote:
            reporter.post_updates_note(issues)
    if issues:
        reporter.write_json(issues)
        reporter.write_flat_json(reporter.flatten(issues))
        reporter.write_overview_html(reporter.flatten(issues))
        reporter.write_stats_html(reporter.flatten(issues))
        helper = Tracking_Helper(args)
        helper.write_yaml(issues, reporter.project_metadata)
    else:
        print("No issues found.")

        #TODO: get notes to check for salesforce links
        #TODO: provide high level monitoring / metrics
    #Moonshot: generate a customer profile from issue labels
      # similarity with bag of words approaches like doc2vec, LSA or similar consine similarity measures
      # issue similarity or customer similarity?
      # may need more data about customer
