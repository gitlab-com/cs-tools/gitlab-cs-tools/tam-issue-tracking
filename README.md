# TAM issue tracking

Track issues for customers, visualize and filter, automatically update and get notifications!

## Features

This project will can be used to track, visualize and update on GitLab issues. It will create [an issue](https://gitlab.com/gitlab-com/cs-tools/gitlab-cs-tools/tam-issue-tracking/-/issues/1) and a [pages site](https://gitlab-com.gitlab.io/cs-tools/gitlab-cs-tools/tam-issue-tracking/index.html) including a [dashboard](https://gitlab-com.gitlab.io/cs-tools/gitlab-cs-tools/tam-issue-tracking/stats.html) that list issues tracked in a [yml file](https://gitlab.com/gitlab-com/cs-tools/gitlab-cs-tools/tam-issue-tracking/-/blob/master/issues.yml).

It will also provide regular updates on issue events (changed state, milestone or labels) in the issue.

This will keep you up to date about the state of your customer's issues while providing a nice interface, for example for EBRs or cadence calls.

The script provides a way to add feature requests for tracking by opening an issue with the `feature_request` issue template on the project. Once the issue has been opened, the data added to the feature request issue will automatically be added to the `issues.yml` file for tracking.

## Set up

This project provides a docker image as well as a CI template for ease of use.

1. Configure a project access token in your project. It needs `api`, `read_registry` and `write_repository` rights with a `maintainer` role. Go to `Settings->CI/CD->Variables`. Create a protected, masked variable "GIT_TOKEN" and paste your project access token.
2. Create a `.gitlab-ci.yml` file including the template and adjusting the variables:

```
include: https://gitlab.com/gitlab-com/cs-tools/gitlab-cs-tools/tam-issue-tracking/-/raw/master/track_issues.yml

variables:
  PROJECT: $project_ID
  CADENCE: $update_cadence
  POST_UPDATES: "False"
  CUSTOMER_NAME: $customer_name
  ISSUES_FILE: "issues.yml"
```

3. On first run, this script will bootstrap itself. It will crawl all GitLab issues linked in the project's issues and use them to create the `issues.yml`. It will create a tracking issue and a scheduled pipeline for regular execution. It will then start a pipeline to update the issues and deploy the pages site.
4. Navigate to the issue labelled "product-issues" and turn on notifications to get regular updates.
5. Confirm that Pages Access Level is `Only Project Members` in `Settings->General->Visibility`.
6. Make sure to update the `metadata` in the `issues.yml`.

## Configuration

* PROJECT: ID of the project where the tracking issue will be created and updated
* CADENCE: Number of days interval for scheduled pipeline to run and changes note to be posted
* POST_UPDATES: If the pipeline will post an update notes to the issue or not. Recommended as `FALSE` for the standard pipeline (run when a new issue is added the yml file) and `TRUE` for the scheduled or manually run one
* CUSTOMER_NAME: Name of the customer, used as issue title and on the pages site
* ISSUES_FILE: Filename of the yml file containing issue links. Best to leave as `issues.yml`.

## YML format

The issue file format is:

```
issues:
  https://gitlab.com/gitlab-org/gitlab/-/issues/id:
    priority: int
    date_logged: yyyy-mm-dd
    notes: "string"
  https://gitlab.com/gitlab-org/gitlab/-/issues/id:
    ...

metadata:
  sa: 'SA link'
  sales: 'SAL link'
  salesforce: 'Salesforce link'
  tam: 'TAM link'
```

Additional fields for the issues (priority, date_logged, notes) are optional, so this works as well:

```
issues:
  https://gitlab.com/gitlab-org/gitlab/-/issues/id:
  https://gitlab.com/gitlab-org/gitlab/-/issues/id:

metadata:
  sa: 'SA link'
  sales: 'SAL link'
  salesforce: 'Salesforce link'
  tam: 'TAM link'
```

Basically, you can just drop the issue link in the issues array. However, the additional fields offer filter functionality and can be useful to calculate cycle time and offer priority information to product.
