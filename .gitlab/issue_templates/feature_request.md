### Overview:

#### Required Information

| | |
| -- | -- |
|  **Feature Request Issue link** |  https://gitlab.com/gitlab-org/gitlab/-/issues/XXXXX  |
|  **Why interested** |      |
|  **Current solution for this problem** |     |
|  **How valuable to customer** |  1  |
|  **How urgent** |  1  |
|  **Questions** |    |

___

#### TAM Tasks

  * [ ] Complete above information.  

/label ~"Feature Request" 
