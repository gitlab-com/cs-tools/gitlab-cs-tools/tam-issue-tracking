FROM python:3.7

WORKDIR /report

COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt

COPY issues-report.py /report/issues-report.py
COPY tracking_helper.py /report/tracking_helper.py
COPY public /report/public
COPY template /report/template
COPY .gitlab/issue_templates/feature_request.md /report/feature_request.md
COPY gitlab_issue_map.json /report/gitlab_issue_map.json
