#!/usr/bin/env python3

import gitlab
import json
import csv
import os
import argparse
import re
import urllib.parse
import time
import yaml
import base64
import requests
import datetime
from datetime import datetime, timedelta
from distutils import util

class Tracking_Helper():

    gitlab_url = "https://gitlab.com"
    api_url = "https://gitlab.com/api/v4/"
    #gitlab-org/gitlab, where most issues are moved to
    gitlab_org_project = None
    gitlab_moved_issues_map = None
    issuefile = None
    cadence = 7
    headers = None
    gl = None
    token = None
    headers = None
    project = None
    default_branch = "main"
    customer_project_name = ""
    path = os.path.abspath(os.path.dirname(__file__))
    ISSUE_FIELDS = ["web_url","title","labels","milestone","state","updated_at","created_at","closed_at"]

    def __init__(self, args):
        # won't always be set, defaults are good enough
        try:
            if args.gitlab:
                self.gitlab_url = args.gitlab if args.gitlab.endswith("/") else args.gitlab + "/"
                self.api_url = self.gitlab_url + "api/v4/"
            self.cadence = args.cadence
        except:
            pass
        self.token = args.token
        self.gl = gitlab.Gitlab(self.gitlab_url, private_token=self.token)
        self.headers = {'PRIVATE-TOKEN': args.token}
        self.project = self.gl.projects.get(args.project)
        self.customer_project_name = self.project.attributes["path_with_namespace"][self.project.attributes["path_with_namespace"].rfind("/")+1:]
        self.issuefile = args.issuefile
        #does only work in subgroups, not personal namespaces. Should be fine
        namespace = self.project.namespace["full_path"]
        self.gitlab_org_project = self.gl.projects.get(278964)
        self.headers = {'PRIVATE-TOKEN': args.token}
        self.default_branch = self.get_default_branch()
        print("Default branch is %s" % self.default_branch)
        # I can't believe I'm doing this
        # Parse a map of gitlab issues so I can get issues by id to deal with moved issues
        with open(self.path + "/gitlab_issue_map.json","r") as infile:
            self.gitlab_moved_issues_map = json.load(infile)

    def get_default_branch(self):
        branches = self.project.branches.list()
        default_branch = self.default_branch
        for branch in branches:
            if branch.attributes.get("default"):
                default_branch = branch.attributes["name"]
        return default_branch

    def get_issues(self, project):
        print("Parsing issues for %s" % self.customer_project_name)
        #tracking_issues = project.issues.list(labels=['product-issues'], as_list=False)
        tracking_issues = project.issues.list(as_list=False)
        extracted_issues = []
        if not tracking_issues:
            print("No issues found.")
            return extracted_issues
        for issue in tracking_issues:
            extracted_issues.extend(self.extract_issues(issue))
        extracted_issues = self.filter_duplicates(extracted_issues)
        customer_issues = {}
        for issue in extracted_issues:
            updated_date = self.datetime_to_isostring(issue.updated_at)
            customer_issues[issue.web_url] = {"object":issue,"priority":0,"date_logged":updated_date,"notes":""}
        return customer_issues

    def get_metadata(self, project):
        readme = ""
        metadata = {}
        metadata["tam"] = ""
        metadata["sa"] = ""
        metadata["sales"] = ""
        metadata["salesforce"] = ""
        try:
            items = project.repository_tree()
            for item in items:
                if "README" in item["name"]:
                    file_info = project.repository_blob(item["id"])
                    readme = base64.b64decode(file_info["content"])
            if not readme:
                return metadata
        except:
            return metadata

        readme = readme.decode('utf-8')
        salesforce_regex = "https:\/\/[a-z0-9\.]+\.salesforce.com/[A-Za-z0-9]*"
        try:
            metadata["salesforce"] = re.findall(salesforce_regex, readme)[0].strip()
            if metadata["salesforce"].strip() == "https://gitlab.my.salesforce.com/":
                metadata["salesforce"] = ""
        except:
            #print("ERROR: No salesforce link for %s" % project.attributes["path_with_namespace"][30:])
            pass
        account_regex = "([Aa]ccount.*([Ll]eader|[Ee]xecutive|[Mm]anager).*|Solutions Architect.*|SAL.*?|SA .*?|TAM.*?)(:|\|)"

        for line in readme.splitlines():
            if re.findall(account_regex, line):
                delimiter = ":"
                #it's a table
                if line.find("|") == 0:
                    table = True
                    line = line[1:]
                    delimiter = "|"
                account_team_string = line[line.find(delimiter)+1:]
                account_team_string = account_team_string.replace("|","").strip()
                if "[" in account_team_string:
                    account_team_string = account_team_string[account_team_string.find("[")+1:account_team_string.find("]")]
                if "technical" in line.lower():
                    metadata["tam"] = account_team_string.strip()
                elif "solution" in line.lower():
                    metadata["sa"] = account_team_string.strip()
                elif "salesforce" not in line.lower():
                    metadata["sales"] = account_team_string.strip()
        return metadata

    #legacy method to extract from description
    def parse_description(self, issue, project_map):
        issue_regex = "https:\/\/gitlab\.com/gitlab-org/[a-zA-Z0-9_\-\.]+(\/-)?\/issues/[0-9]+"
        issue_iter = re.finditer(issue_regex,issue.description)
        parsed_issues = set()

        for issue_match in issue_iter:
            parsed_issues.add(issue_match.group())
            # can use issue_match.span() to get string position and search backwards for priority labels
        issue_objects = []
        for parsed_issue in parsed_issues:
            issue = self.fetch_issue(parsed_issue, project_map)
            if issue:
                issue_objects.append(issue)
        return issue_objects

    #legacy method to extract issues from description text
    def extract_issues(self,issue):
        customer = self.project.attributes["path_with_namespace"][30:]
        customer_issues = []
        gitlab_issues = []
        project_map = {}
        try:
            issue_links = issue.links.list()
            for linked_issue in issue_links:
                fetched_issue = self.fetch_issue(linked_issue.attributes["web_url"], project_map)
                if fetched_issue:
                    gitlab_issues.append(fetched_issue)
        except Exception as e:
            print("ERROR: {0}".format(e))
            print("Could not retrieve linked issues of %s for customer %s" % (issue.attributes["web_url"], customer))
        if issue.description:
            issues_in_text = self.parse_description(issue, project_map)
            if issues_in_text:
                gitlab_issues.extend(issues_in_text)
        return gitlab_issues

    def create_tracking_issue(self, project):
        tracking_issues = project.issues.list(labels=['product-issues'], as_list=False)
        if not tracking_issues:
            print("No tracking issue found, creating...")
            issue = project.issues.create({'title': 'Feature request tracking',
                                   'description': 'Track GitLab feature requests'})
            issue.labels = ['product-issues']
            issue.save()
            #don't auto-subscribe, otherwise can't use PAT
            #issue.subscribe()
        else:
            print("Tracking issue exists.")

    def create_pipeline_schedule(self, project):
        if not project.pipelineschedules.list():
            print("No pipeline schedule found, creating...")
            cron = "0 0 */%s * *" % self.cadence
            sched = project.pipelineschedules.create({
                'ref': self.default_branch,
                'description': 'Update issues',
                'cron': cron})
            sched.variables.create({'key':'POST_UPDATES','type':'env_var','value':'True'})
        else:
            print("Pipeline schedule found.")

    def set_pages_access_level(self, project):
        print("Validating pages access level...")
        if project.pages_access_level != "private":
            print("Setting pages access level to private")
            project.pages_access_level = "private"
            project.save()
        else:
            print("Pages access level is private.")

    def write_yaml(self, issues, metadata):
        out_data = {"issues":{},"metadata":metadata}
        if not issues:
            out_data["issues"]["https://gitlab.com/gitlab-org/gitlab/issues/1"] = {"urgency":1,"value":1,"date_logged":None,"notes":""}
        else:
            for issue in issues:
                issue_data = {}
                issue_data["urgency"] = issues[issue].get("urgency",1)
                issue_data["value"] = issues[issue].get("value",1)
                issue_data["date_logged"] = issues[issue].get("date_logged")
                issue_data["notes"] = issues[issue].get("notes","")
                if "object" in issues[issue]:
                    attributes = issues[issue]["object"].attributes
                    for field in self.ISSUE_FIELDS:
                        issue_data[field] = attributes.get(field)
                out_data["issues"][issue] = issue_data
        with open(self.path + "/" + self.issuefile,"w") as out_file:
            issueyml = yaml.dump(out_data, out_file)


    def yaml_exists(self, project):
        yml_exists = False
        items = project.repository_tree()
        for item in items:
            if item["name"] == self.issuefile:
                yml_exists = True
        return yml_exists

    def push_yaml(self, project, yml_exists, ci_skip = False):
        action = "create"
        if yml_exists:
            action = "update"
        commit_message = "[ci skip] push issues.yml" if ci_skip else "push issues.yml"
        data = {
            'branch': self.default_branch,
            'commit_message': commit_message,
            'actions': [
                {
                    'action': action,
                    'file_path': 'issues.yml',
                    'content': open(self.path + "/" + self.issuefile).read(),
                }
            ]
        }
        commit = project.commits.create(data)

    # get python-gitlab issue object, no matter from which project (even ce/ee), resolve moved issues
    def fetch_issue(self, issue_url, project_map = {}):
        if "gitlab-org" not in issue_url:
            return None
        issue_url = issue_url[0:-1] if issue_url.endswith("/") else issue_url
        project_id = issue_url[issue_url.find("gitlab-org/"):issue_url.rfind("issues")-1].replace("/-","").replace("gitlab-ee","gitlab").replace("gitlab-ce","gitlab-foss")
        parsed_issue_id = issue_url[issue_url.rfind("/")+1:]
        try:
            project = None
            if project_id not in project_map:
                project = self.gl.projects.get(project_id)
                project_map[project_id] = project
            else:
                project = project_map[project_id]

            issue_object = project.issues.get(parsed_issue_id)
            #handle moved issues
            issue_object = self.resolve_moved_issue(issue_object)
        except Exception as e:
            print("ERROR: {0}".format(e))
            print("Project: %s" % project_id)
            print("GitLab issue not found: %s" % issue_url)
        return issue_object

    def resolve_moved_issue(self, issue):
        if issue.attributes["moved_to_id"]:
            if str(issue.attributes["moved_to_id"]) in self.gitlab_moved_issues_map:
                target_issue_iid = self.gitlab_moved_issues_map[str(issue.attributes["moved_to_id"])]
                target_issue_object = self.gitlab_org_project.issues.get(target_issue_iid)
                issue = target_issue_object
        return issue

    def filter_duplicates(self, issue_objects):
        unique_urls = set()
        unique_issues = []
        for issue in issue_objects:
            if issue.attributes["web_url"] not in unique_urls:
                unique_urls.add(issue.attributes["web_url"])
                unique_issues.append(issue)
        return unique_issues

    def datetime_to_isostring(self, datetime_string):
        isostring = ""
        try:
            isostring = str(datetime.strptime(datetime_string, '%Y-%m-%dT%H:%M:%S.%fZ').date().isoformat())
        except:
            pass
        return isostring

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Create report for GitLab issues')
    parser.add_argument('token', help='API token able to read the requested projects')
    parser.add_argument('project', help='Project ID to crawl for issues')
    parser.add_argument('issuefile', help='File name of the issues file')
    parser.add_argument('cadence', help='How often the scheduled pipeline should run in days')
    parser.add_argument('--gitlab', help='GitLab URL, defaults to https://gitlab.com/')
    # flag needed to push file after pages job has run. Otherwise pages will fail
    # because last commit sha will be different from the one that triggered pages pipeline
    parser.add_argument('--push_only', help='Only push existing issues file.', action="store_true")

    args = parser.parse_args()
    helper = Tracking_Helper(args)

    yml_exists = helper.yaml_exists(helper.project)
    if not args.push_only:
        # no issues file exists, bootstrap it from existing issues
        if not yml_exists:
            print("No yml file in repo, bootstrapping from existing projects")
            issues = helper.get_issues(helper.project)
            metadata = helper.get_metadata(helper.project)
            helper.write_yaml(issues, metadata)
        #these will simply do nothing if the setup is already correct
        print("Validating config")
        helper.create_tracking_issue(helper.project)
        helper.create_pipeline_schedule(helper.project)
        helper.set_pages_access_level(helper.project)
    else:
        helper.push_yaml(helper.project, yml_exists, True)

